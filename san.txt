Git global setup
git config --global user.name "santosh kumar"
git config --global user.email "bhsantosh1989@gmail.com"

Create a new repository
git clone https://gitlab.com/bhsantosh1/newtest.git
cd newtest
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/bhsantosh1/newtest.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/bhsantosh1/newtest.git
git push -u origin --all
git push -u origin --tags